﻿using AutoBogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using XmlSerializer.Common.Models;
using XmlSerializer.Common.Tests;
using XmlSerializer.Server.Services;
using Xunit;

namespace XmlSerializer.Server.Tests.Services
{
    public class XmlServiceTests
    {
        [Fact]
        public void ToXElement_VisitsNotNull_CreatesRequiredElements()
        {
            var sut = new XmlService();
            var requestModel = new RequestModelFaker().Generate("Default, visits");

            var xElement = sut.ToXElement(requestModel);

            var ixElement = xElement.Element("ix");
            Assert.NotNull(ixElement);
            Assert.Equal(requestModel.Index.ToString(), ixElement.Value);

            var nameElement = xElement.Element("content")?.Element("name");
            Assert.NotNull(nameElement);
            Assert.Equal(requestModel.Name, nameElement.Value);

            var ixVisitsElement = xElement.Element("content")?.Element("visits");
            Assert.NotNull(ixVisitsElement);
            Assert.Equal(requestModel.Visits.ToString(), ixVisitsElement.Value);

            var ixDateRequestedElement = xElement.Element("content")?.Element("dateRequested");
            Assert.NotNull(ixDateRequestedElement);
            Assert.Equal(requestModel.Date.ToString("yyyy-MM-dd"), ixDateRequestedElement.Value);
        }

        [Fact]
        public void ToXElement_VisitsIsNull_DoesNotCreateVisitsElement()
        {
            var sut = new XmlService();
            var requestModel = new RequestModelFaker().Generate("Default, no-visits");

            var xElement = sut.ToXElement(requestModel);

            var ixVisitsElement = xElement.Element("content")?.Element("visits");
            Assert.Null(ixVisitsElement);
        }

        [Fact]
        public void ToString_Always_AddsXmlDeclarationAndIndents()
        {
            var sut = new XmlService();
            var element = new XElement("root", new XElement("tag", "text"));

            var result = sut.ToString(element);

            Assert.Equal(
                @"<?xml version=""1.0"" encoding=""utf-16""?>
<root>
  <tag>text</tag>
</root>",
                result);
        }
    }
}
