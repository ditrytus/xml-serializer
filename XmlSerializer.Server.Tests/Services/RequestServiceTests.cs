﻿using Hangfire;
using Hangfire.Common;
using Hangfire.States;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using XmlSerializer.Common.Models;
using XmlSerializer.Common.Tests;
using XmlSerializer.Server.Repositories;
using XmlSerializer.Server.Services;
using Xunit;

namespace XmlSerializer.Server.Tests.Services
{
    public class RequestServiceTests
    {
        [Fact]
        public async Task AddRequests_Always_InsertsRequests()
        {
            var repository = Substitute.For<IRequestRepository>();
            var backgrondJob = Substitute.For<IBackgroundJobClient>();
            var xmlService = Substitute.For<IXmlService>();
            var fileService = Substitute.For<IFileService>();
            var sut = new RequestService(repository, backgrondJob, xmlService, fileService);

            var requests = new RequestModelFaker().Generate(3).ToArray();
            await sut.AddRequests(requests);

            await repository.Received().InsertRequests(requests);
        }

        [Fact]
        public void EnqueueSaveAllRequestsAsXml_Always_EnqueuesSaveAllRequestsAsXml()
        {
            var repository = Substitute.For<IRequestRepository>();
            var backgrondJob = Substitute.For<IBackgroundJobClient>();
            var xmlService = Substitute.For<IXmlService>();
            var fileService = Substitute.For<IFileService>();
            var sut = new RequestService(repository, backgrondJob, xmlService, fileService);

            sut.EnqueueSaveAllRequestsAsXml();

            backgrondJob
                .Received()
                .Create(
                    Arg.Is<Job>(job => job.Method.Name == "SaveAllRequestsAsXml"),
                    Arg.Any<IState>());
        }

        [Fact]
        public void SaveAllRequestsAsXml_NoUnsavedRequests_NoJobsAreEnqueued()
        {
            var repository = Substitute.For<IRequestRepository>();
            var backgrondJob = Substitute.For<IBackgroundJobClient>();
            var xmlService = Substitute.For<IXmlService>();
            var fileService = Substitute.For<IFileService>();
            var sut = new RequestService(repository, backgrondJob, xmlService, fileService);

            repository.GetUnsavedRequests(Arg.Any<int>()).Returns(new RequestModel[0]);

            sut.SaveAllRequestsAsXml();

            backgrondJob
                .DidNotReceive()
                .Create(
                    Arg.Is<Job>(job => job.Method.Name == "SaveRequestAsXml"),
                    Arg.Any<IState>());
        }

        [Fact]
        public void SaveAllRequestsAsXml_NoUnsavedRequests_NoRequestsAreUpdated()
        {
            var repository = Substitute.For<IRequestRepository>();
            var backgrondJob = Substitute.For<IBackgroundJobClient>();
            var xmlService = Substitute.For<IXmlService>();
            var fileService = Substitute.For<IFileService>();
            var sut = new RequestService(repository, backgrondJob, xmlService, fileService);

            repository.GetUnsavedRequests(Arg.Any<int>()).Returns(new RequestModel[0]);

            sut.SaveAllRequestsAsXml();

            repository
                .DidNotReceive()
                .UpdateRequests(Arg.Any<RequestModel[]>());
        }

        [Fact]
        public void SaveAllRequestsAsXml_UnsavedRequestsExist_JobIsCreatedForEveryRequest()
        {
            var repository = Substitute.For<IRequestRepository>();
            var backgrondJob = Substitute.For<IBackgroundJobClient>();
            var xmlService = Substitute.For<IXmlService>();
            var fileService = Substitute.For<IFileService>();
            var sut = new RequestService(repository, backgrondJob, xmlService, fileService);

            var requests = new RequestModelFaker().Generate(3).ToArray();
            repository.GetUnsavedRequests(Arg.Any<int>()).Returns(requests, new[] { new RequestModel[0] });

            sut.SaveAllRequestsAsXml();

            backgrondJob
                .Received(requests.Length)
                .Create(
                    Arg.Is<Job>(job =>
                        job.Method.Name == "SaveRequestAsXml" &&
                        requests
                            .Select(r => r.Id)
                            .Contains((int)job.Args[0])),
                    Arg.Any<IState>());
        }

        [Fact]
        public void SaveAllRequestsAsXml_UnsavedRequestsExist_RequestsAreUpdatedAsSaved()
        {
            var repository = Substitute.For<IRequestRepository>();
            var backgrondJob = Substitute.For<IBackgroundJobClient>();
            var xmlService = Substitute.For<IXmlService>();
            var fileService = Substitute.For<IFileService>();
            var sut = new RequestService(repository, backgrondJob, xmlService, fileService);

            var requests = new RequestModelFaker().Generate(3).ToArray();
            repository.GetUnsavedRequests(Arg.Any<int>()).Returns(requests, new[] { new RequestModel[0] });

            sut.SaveAllRequestsAsXml();

            repository
                .Received()
                .UpdateRequests(
                    Arg.Is<RequestModel[]>(rs => rs.All(r => r.WasSaved)));
        }

        [Fact]
        public void SaveRequestAsXml_Always_SavesXmlStringInFileUnderPath()
        {
            var repository = Substitute.For<IRequestRepository>();
            var backgrondJob = Substitute.For<IBackgroundJobClient>();
            var xmlService = Substitute.For<IXmlService>();
            var fileService = Substitute.For<IFileService>();
            var sut = new RequestService(repository, backgrondJob, xmlService, fileService);

            int requestId = 102;
            var request = new RequestModelFaker().Generate();
            var xelement = new XElement("node");

            repository.GetById(requestId).Returns(request);
            xmlService.ToXElement(request).Returns(xelement);
            xmlService.ToString(xelement).Returns("<node></node>");
            fileService.GetPath(request).Returns("a/path/to/file");

            sut.SaveRequestAsXml(requestId);

            fileService.Received().SaveTextFile("a/path/to/file", "<node></node>");
        }
    }
}
