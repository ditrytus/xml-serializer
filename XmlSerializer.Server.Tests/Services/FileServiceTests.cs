﻿using NSubstitute;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XmlSerializer.Common.Models;
using XmlSerializer.Server.Services;
using Xunit;

namespace XmlSerializer.Server.Tests.Services
{
    public class FileServiceTests
    {
        private FileService sut;

        public FileServiceTests()
        {
            var path = Substitute.For<Paths>();
            path.DataBaseFolderPath.Returns("base_url");

            sut = new FileService(path);
        }

        [Fact]
        public void GetPath_Always_ComposesPath()
        {
            RequestModel request = new RequestModel()
            {
                Id = 23,
                Date = new DateTime(1987, 8, 18)
            };

            var result = sut.GetPath(request);

            Assert.Equal(@"base_url\xml\1987-08-18\23.xml", result);
        }

        [Fact]
        public void SaveTextFile_FileAlreadyExists_DoesNotChangeFile()
        {
            var tempFileName = Path.GetTempFileName();
            var oldWriteTime = File.GetLastWriteTimeUtc(tempFileName);

            sut.SaveTextFile(tempFileName, "Lorem ipsum");

            var newWriteTime = File.GetLastWriteTimeUtc(tempFileName);
            Assert.Equal(oldWriteTime, newWriteTime);

            File.Delete(tempFileName);
        }

        [Fact]
        public void SaveTextFile_FolderDoesNotExists_CreatesFolder()
        {
            var tempDir = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
            var tempFile = Path.Combine(tempDir, "file.txt");

            Assert.False(Directory.Exists(tempDir));

            sut.SaveTextFile(tempFile, "Lorem ipsum");

            Assert.True(Directory.Exists(tempDir));

            Directory.Delete(tempDir, true);
        }

        [Fact]
        public void SaveTextFile_FileDoesNotExist_CreatesNewFileWithContent()
        {
            var tempFile = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
            var originalContent = "Lorem ipsum";

            Assert.False(File.Exists(tempFile));

            sut.SaveTextFile(tempFile, originalContent);

            Assert.True(File.Exists(tempFile));
            var readContent = File.ReadAllText(tempFile);
            Assert.Equal(originalContent, readContent);

            File.Delete(tempFile);
        }
    }
}
