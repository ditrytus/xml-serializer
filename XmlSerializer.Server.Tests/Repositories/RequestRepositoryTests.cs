﻿using Bogus;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XmlSerializer.Common.Models;
using XmlSerializer.Common.Tests;
using XmlSerializer.Server.Data;
using XmlSerializer.Server.Repositories;
using Xunit;

namespace XmlSerializer.Server.Tests.Repositories
{
    public class RequestRepositoryTests
    {
        [Fact]
        public async Task InsertRequests_Always_InsertsRequests()
        {
            var faker = new RequestModelFaker();
            var data = faker.Generate(3, "Default, id").ToList();

            var set = Substitute
                .For<DbSet<RequestModel>, IQueryable<RequestModel>, IDbAsyncEnumerable<RequestModel>>()
                .SetupData(data);

            var context = Substitute.For<RequestsContext>();
            context.Requests.Returns(set);

            var sut = new RequestRepository(() => context);

            var requests = faker.Generate(3).ToArray();

            await sut.InsertRequests(requests);

            Assert.True(requests.All(r => data.Contains(r)));
            await context.Received().SaveChangesAsync();
        }

        [Fact]
        public void GetUnsavedRequests_CountLessThanUnsaved_ReturnsCountOfUnsaved()
        {
            var faker = new RequestModelFaker();
            var saved = faker.Generate(3, "Default, id, saved").ToList();
            var unsaved = faker.Generate(3, "Default, id, unsaved").ToList();

            Assert.All(unsaved, r => Assert.False(r.WasSaved));

            var data = saved.Concat(unsaved).ToList();

            var set = Substitute
                .For<DbSet<RequestModel>, IQueryable<RequestModel>, IDbAsyncEnumerable<RequestModel>>()
                .SetupData(data);

            var context = Substitute.For<RequestsContext>();
            context.Requests.Returns(set);

            var sut = new RequestRepository(() => context);

            var result = sut.GetUnsavedRequests(2);

            Assert.True(result.All(r => unsaved.Contains(r)));
            Assert.All(result, r => Assert.False(r.WasSaved));
        }

        [Fact]
        public void GetUnsavedRequests_CountGreaterThanUnsaved_ReturnsAllUnsaved()
        {
            var faker = new RequestModelFaker();
            var saved = faker.Generate(3, "Default, id, saved").ToList();
            var unsaved = faker.Generate(3, "Default, id, unsaved").ToList();

            Assert.All(unsaved, r => Assert.False(r.WasSaved));

            var data = saved.Concat(unsaved).ToList();

            var set = Substitute
                .For<DbSet<RequestModel>, IQueryable<RequestModel>, IDbAsyncEnumerable<RequestModel>>()
                .SetupData(data);

            var context = Substitute.For<RequestsContext>();
            context.Requests.Returns(set);

            var sut = new RequestRepository(() => context);

            var result = sut.GetUnsavedRequests(7);

            Assert.True(result.All(r => unsaved.Contains(r)));
            Assert.True(unsaved.All(r => result.Contains(r)));
        }

        [Fact]
        public void GetById_RequestExists_ReturnsRequest()
        {
            var faker = new RequestModelFaker();
            var data = faker.Generate(3, "Default, id").ToList();

            var set = Substitute
                .For<DbSet<RequestModel>, IQueryable<RequestModel>, IDbAsyncEnumerable<RequestModel>>()
                .SetupData(data);

            var context = Substitute.For<RequestsContext>();
            context.Requests.Returns(set);

            var sut = new RequestRepository(() => context);

            var expected = data.First();

            var result = sut.GetById(expected.Id);

            Assert.Equal(result, expected);
        }

        [Fact]
        public void GetById_RequestDoesNotExist_ReturnsRequest()
        {
            var faker = new RequestModelFaker();
            var data = faker.Generate(3, "Default, id").ToList();

            var set = Substitute
                .For<DbSet<RequestModel>, IQueryable<RequestModel>, IDbAsyncEnumerable<RequestModel>>()
                .SetupData(data);

            var context = Substitute.For<RequestsContext>();
            context.Requests.Returns(set);

            var sut = new RequestRepository(() => context);

            var result = sut.GetById(224456);

            Assert.Null(result);
        }
    }
}
