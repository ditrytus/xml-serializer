﻿using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XmlSerializer.Common.Tests;
using XmlSerializer.Server.Controllers;
using XmlSerializer.Server.Services;
using Xunit;

namespace XmlSerializer.Server.Tests.Controllers
{
    public class DataControllerTests
    {
        [Fact]
        public async Task Index_Always_AddsRequests()
        {
            var requestsService = Substitute.For<IRequestService>();
            var sut = new DataController(requestsService);
            var requests = new RequestModelFaker().Generate(3).ToArray();

            await sut.Index(requests);

            await requestsService.Received().AddRequests(requests);
        }
    }
}
