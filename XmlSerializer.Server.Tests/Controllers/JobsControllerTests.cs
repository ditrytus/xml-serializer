﻿using NSubstitute;
using XmlSerializer.Server.Controllers;
using XmlSerializer.Server.Services;
using Xunit;

namespace XmlSerializer.Server.Tests.Controllers
{
    public class JobsControllerTests
    {
        [Fact]
        public void SaveFiles_Always_EnqueuesSaveAllRequestsAsXml()
        {
            var requestsService = Substitute.For<IRequestService>();
            var sut = new JobsController(requestsService);

            sut.SaveFiles();

            requestsService.Received().EnqueueSaveAllRequestsAsXml();
        }
    }
}
