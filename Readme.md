# Notes on the implementation

1. Specification does not give any details on constraints on RequestModel beside the C# datatypes. Lack of such constraints lead to not implementing separate validation layer except just validating the types and DB type for `RequestModel.Name` is `nvarchar(max)`.

2. All layers of API are put in the single assembly because of a small size of the project.

3. Background jobs are implemented using Hangfire framework. http://hangfire.io. Dashboard is available locally under `/hangfire` path

4. There is no separate runner for migrations. DB will be created and upgreaded on application startup.

# Troubleshooting:

1. xUnit tests are not running

I did encounter a bug in xUnit VS Runner which caused that tests were not discovered after the initial clone of the project.

*Short-term solution* - System restart. Just restarting VS does not help.

*Long-term solution* - To implement xUnit Console Runner and provide a script to quickly execute all tests.