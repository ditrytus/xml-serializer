﻿using AutoBogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XmlSerializer.Common.Models;

namespace XmlSerializer.Common.Tests
{
    public class RequestModelFaker : AutoFaker<RequestModel>
    {
        public RequestModelFaker()
        {
            StrictMode(true);
            Ignore(r => r.Id);
            Ignore(r => r.WasSaved);
            RuleFor(r => r.Index, f => f.Random.Number(0, 100000));
            RuleFor(r => r.Name, f => string.Join(" ", f.Lorem.Words(3)));
            RuleFor(r => r.Visits, f => f.Random.Number(1, 10) == 1 ? (int?)null : f.Random.Number(10, 1000));
            RuleFor(r => r.Date, f => f.Date.Past(6).Date);

            RuleSet("visits", ruleSet =>
            {
                ruleSet.RuleFor(r => r.Visits, f => f.Random.Number(10, 1000));
            });

            RuleSet("no-visits", ruleSet =>
            {
                ruleSet.RuleFor(r => r.Visits, f => (int?)null);
            });

            int id = 0;

            RuleSet("id", ruleSet =>
            {
                ruleSet.RuleFor(r => r.Id, f => ++id);
            });

            RuleSet("saved", ruleSet =>
            {
                ruleSet.RuleFor(r => r.WasSaved, f => true);
            });

            RuleSet("unsaved", ruleSet =>
            {
                ruleSet.RuleFor(r => r.WasSaved, f => false);
            });
        }
    }
}
