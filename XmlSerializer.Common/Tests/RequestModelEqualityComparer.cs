﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XmlSerializer.Common.Models;

namespace XmlSerializer.Common.Tests
{
    public class RequestModelEqualityComparer : IEqualityComparer<RequestModel>
    {
        public bool Equals(RequestModel x, RequestModel y)
        {
            return y.Index == x.Index &&
                y.Name == x.Name &&
                y.Visits == x.Visits &&
                y.WasSaved == x.WasSaved;
        }

        public int GetHashCode(RequestModel obj)
        {
            return obj.Index.GetHashCode() ^
                obj.Name.GetHashCode() ^
                obj.Visits.GetHashCode() ^
                obj.WasSaved.GetHashCode();
        }
    }
}
