﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XmlSerializer.Common.Models
{
    public class RequestModel
    {
        [JsonIgnore]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "ix")]
        public int Index { get; set; }

        public string Name { get; set; }

        public int? Visits { get; set; }

        public DateTime Date { get; set; }

        [JsonIgnore]
        public bool WasSaved { get; set; }
    }

}