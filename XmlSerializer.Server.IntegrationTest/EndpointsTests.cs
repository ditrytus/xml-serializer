﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using XmlSerializer.Common.Tests;
using XmlSerializer.Server.Data;
using Xunit;

namespace XmlSerializer.Server.IntegrationTest
{
    public class EndpointsTests : IClassFixture<ServerFixture>
    {
        private ServerFixture fixture;

        public EndpointsTests(ServerFixture fixture)
        {
            this.fixture = fixture;
        }

        private Func<RequestsContext> GetCreateContextFunc() =>
            () => new RequestsContext(fixture.ConnectionString);

        [Fact]
        public async Task PostData_Always_InsertsToDatabase()
        {
            var totalCount = GetCreateContextFunc()().Requests.Count();

            var requests = new RequestModelFaker().GenerateLazy(10).ToArray();
            var client = new HttpClient();
            var result = await client.PostAsJsonAsync($"{ServerFixture.BaseUrl}/api/data", requests);

            var allDbRequests = GetCreateContextFunc()().Requests.ToList();

            allDbRequests.Should().HaveCount(totalCount + 10);
            Assert.True(
                requests.All(
                    r => allDbRequests.Contains(r, new RequestModelEqualityComparer())));
        }

        [Fact]
        public async Task GetSaveFiles_Always_CreatesXmlForAllUnsaved()
        {
            var unsaved = GetCreateContextFunc()().Requests.Where(x => !x.WasSaved).ToList();

            CancellationTokenSource source = new CancellationTokenSource();

            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.IncludeSubdirectories = true;
            watcher.Path = fixture.DataFolder;
            watcher.NotifyFilter = NotifyFilters.FileName;
            watcher.Filter = "*.xml";
            watcher.Created += (s, e) =>
            {
                var saved = unsaved.SingleOrDefault(x => x.Id == int.Parse(Path.GetFileNameWithoutExtension(e.FullPath)));
                unsaved.Remove(saved);
                if (!unsaved.Any())
                {
                    source.Cancel();
                }
            };
            watcher.EnableRaisingEvents = true;

            var client = new HttpClient();
            var result = await client.GetAsync($"{ServerFixture.BaseUrl}/api/jobs/saveFiles");

            try
            {
                await Task.Delay(TimeSpan.FromSeconds(30), source.Token);
            }
            catch (TaskCanceledException) { }

            Assert.Empty(unsaved);
        }
    }
}
