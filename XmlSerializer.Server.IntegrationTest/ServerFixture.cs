﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XmlSerializer.Server.DatabaseTests;
using XmlSerializer.Server.Services;
using XmlSerializer.Server.Tests.Repositories;

namespace XmlSerializer.Server.IntegrationTest
{
    public class ServerFixture : DatabaseFixture
    {
        public const string BaseUrl = "http://localhost:9443/";

        public string DataFolder { get; }

        private static IDisposable webApp;

        public ServerFixture()
        {
            DataFolder = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
            Directory.CreateDirectory(DataFolder);
            Paths.DefaultDataBaseFolderPath = DataFolder;

            webApp = WebApp.Start<Startup>(BaseUrl);
        }

        public override void Dispose()
        {
            Directory.Delete(DataFolder, true);
            webApp.Dispose();
            base.Dispose();
        }
    }
}
