﻿using DbUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XmlSerializer.Common.Models;
using XmlSerializer.Common.Tests;
using XmlSerializer.Server.Data;
using XmlSerializer.Server.Repositories;
using Xunit;
using FluentAssertions;

namespace XmlSerializer.Server.DatabaseTests
{
    public class DatabaseTests : IClassFixture<DatabaseFixture>
    {
        DatabaseFixture fixture;

        const int InsertCount = 3;
        const int UpdatedCount = 2;
        const int MinUnsavedCount = DatabaseFixture.NotSavedCount - UpdatedCount;

        public DatabaseTests(DatabaseFixture fixture)
        {
            this.fixture = fixture;
        }

        private Func<RequestsContext> GetCreateContextFunc() =>
            () => new RequestsContext(fixture.ConnectionString);

        [Fact]
        public async Task InsertRequests_Always_InsertsNewRequests()
        {
            var repository = new RequestRepository(GetCreateContextFunc());

            var totalCount = GetCreateContextFunc()().Requests.Count();

            var input = new RequestModelFaker().Generate(InsertCount);
            await repository.InsertRequests(input.ToArray());

            var allRequests = GetCreateContextFunc()().Requests.ToList();

            allRequests.Should().HaveCount(totalCount + InsertCount);
        }

        [Fact]
        public void GetUnsavedRequests_CountLessThanUnsaved_ReturnsCountOfUnsaved()
        {
            var repository = new RequestRepository(GetCreateContextFunc());

            var requests = repository.GetUnsavedRequests(MinUnsavedCount);

            requests.Should().HaveCount(MinUnsavedCount);
        }

        [Fact]
        public void UpdateRequests_Always_UpdatesAllRequests()
        {
            var repository = new RequestRepository(GetCreateContextFunc());

            var requests = fixture.InitialRequests
                .Where(r => !r.WasSaved)
                .Take(UpdatedCount)
                .Select(r => new RequestModel()
                {
                    Date = r.Date,
                    Name = "a new name",
                    Id = r.Id,
                    Index = r.Index,
                    Visits = r.Visits,
                    WasSaved = true
                })
                .ToList();
            
            repository.UpdateRequests(requests.ToArray());

            var context = GetCreateContextFunc()();
            var ids = requests.Select(r => r.Id).ToArray();
            var output = context.Requests.Where(x => ids.Contains(x.Id)).ToArray();

            Assert.All(
                output,
                r =>
                {
                    Assert.Equal("a new name", r.Name);
                    Assert.True(r.WasSaved);
                });
        }

        [Fact]
        public void GetById_RequestExists_ReturnsRequest()
        {
            var repository = new RequestRepository(GetCreateContextFunc());

            var request = repository.GetById(DatabaseFixture.ExistingId);

            Assert.NotNull(request);
            Assert.Equal(DatabaseFixture.ExistingId, request.Id);
        }

        [Fact]
        public void GetById_RequestDoesNotExist_ReturnsRequest()
        {
            var repository = new RequestRepository(GetCreateContextFunc());

            var request = repository.GetById(DatabaseFixture.NonExistingId);

            Assert.Null(request);
        }
    }
}
