﻿using DbUp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XmlSerializer.Common.Models;
using XmlSerializer.Common.Tests;
using XmlSerializer.Server.Data;

namespace XmlSerializer.Server.DatabaseTests
{
    public class DatabaseFixture : IDisposable
    {
        public const int ExistingId = 7;
        public const int NonExistingId = 6345623;

        public string ConnectionString => ConfigurationManager.ConnectionStrings["RequestsContext"].ConnectionString;

        public IEnumerable<RequestModel> InitialRequests { get; }

        public const int SavedCount = 5;
        public const int NotSavedCount = 5;

        public DatabaseFixture()
        {
            EnsureDatabase.For.SqlDatabase(ConnectionString);

            var upgrader =
                DeployChanges.To
                    .SqlDatabase(ConnectionString)
                    .WithScriptsEmbeddedInAssembly(typeof(Migrations.AssemblyMarker).Assembly)
                    .LogToConsole()
                    .Build();

            var result = upgrader.PerformUpgrade();

            var faker = new RequestModelFaker();
            InitialRequests = faker
                .Generate(SavedCount, "Default, saved")
                .Concat(
                    faker.Generate(NotSavedCount, "Default, unsaved"))
                .ToList();

            var context = new RequestsContext(ConnectionString);
            context.Requests.AddRange(InitialRequests);
            context.SaveChanges();
        }

        public virtual void Dispose()
        {
            DropDatabase.For.SqlDatabase(ConnectionString);
        }
    }
}
