using Bogus;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using XmlSerializer.Common.Models;
using XmlSerializer.Common.Tests;

namespace XmlSerializer.Sender
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        static async Task MainAsync(string[] args)
        {
            if (args.Length != 1)
            {
                throw new ArgumentException("Invalid numer of arguments. Requierd 1 integer.");
            }

            int count;
            if (!int.TryParse(args.First(), out count))
            {
                throw new ArgumentException("Argument is not an integer.");
            }

            Console.WriteLine($"Generating {count} requests...");
            var requests = new RequestModelFaker().GenerateLazy(count).ToArray();

            Console.WriteLine($"Sending requests...");
            var client = new HttpClient();
            var result = await client.PostAsJsonAsync($"{ConfigurationManager.AppSettings.Get("APIBaseUrl")}/api/data", requests);

            if (!result.IsSuccessStatusCode)
            {
                Console.WriteLine($"Error sending requests.\nCode: {result.StatusCode}\nContent:\n{await result.Content.ReadAsStringAsync()}");
            }
            else
            {
                Console.WriteLine($"Requests sent.");
            }
        }
    }
}
