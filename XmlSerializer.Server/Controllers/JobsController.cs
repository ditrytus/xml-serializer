﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using XmlSerializer.Server.Services;

namespace XmlSerializer.Server.Controllers
{
    public class JobsController : ApiController
    {
        readonly IRequestService requestService;

        public JobsController(IRequestService requestService)
        {
            this.requestService = requestService;
        }

        [HttpGet]
        public void SaveFiles()
        {
            requestService.EnqueueSaveAllRequestsAsXml();
        }
    }
}
