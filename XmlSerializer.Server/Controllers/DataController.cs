﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using XmlSerializer.Common.Models;
using XmlSerializer.Server.Services;

namespace XmlSerializer.Server.Controllers
{
    public class DataController : ApiController
    {
        readonly IRequestService requestService;

        public DataController(IRequestService requestService)
        {
            this.requestService = requestService;
        }

        [HttpPost]
        public async Task Index(RequestModel[] request)
        {
            await this.requestService.AddRequests(request);
        }
    }
}
