﻿using System;
using System.Linq;
using System.Threading.Tasks;
using XmlSerializer.Common.Models;
using XmlSerializer.Server.Data;

namespace XmlSerializer.Server.Repositories
{
    public class RequestRepository : IRequestRepository
    {
        public RequestRepository()
            : this (() => new RequestsContext())
        {
        }

        public RequestRepository(Func<RequestsContext> createContextFunc)
        {
            this.createContextFunc = createContextFunc;
        }

        private Func<RequestsContext> createContextFunc;

        public async Task InsertRequests(RequestModel[] request)
        {
            using (var db = createContextFunc())
            {
                db.Requests.AddRange(request);
                await db.SaveChangesAsync();
            }
        }

        public RequestModel[] GetUnsavedRequests(int count)
        {
            using (var db = createContextFunc())
            {
                return db.Requests
                    .Where(r => !r.WasSaved)
                    .Take(count)
                    .ToArray();
            }
        }

        public RequestModel GetById(int requestId)
        {
            using (var db = createContextFunc())
            {
                return db.Requests.SingleOrDefault(r => r.Id == requestId);
            }
        }

        public void UpdateRequests(RequestModel[] requests)
        {
            using (var db = createContextFunc())
            {
                foreach(var request in requests)
                {
                    db.Entry(request).State = System.Data.Entity.EntityState.Modified;
                }

                db.SaveChanges();
            }
        }
    }
}