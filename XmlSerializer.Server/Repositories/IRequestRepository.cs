﻿using System;
using System.Threading.Tasks;
using XmlSerializer.Common.Models;

namespace XmlSerializer.Server.Repositories
{
    public interface IRequestRepository
    {
        Task InsertRequests(RequestModel[] request);

        RequestModel[] GetUnsavedRequests(int count);

        RequestModel GetById(int requestId);

        void UpdateRequests(RequestModel[] requests);
    }
}