﻿using Autofac;
using Autofac.Integration.WebApi;
using DbUp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using XmlSerializer.Server.Repositories;
using XmlSerializer.Server.Services;
using Hangfire;
using System.Web;

namespace XmlSerializer.Server
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Database setup
            var connectionString = ConfigurationManager.ConnectionStrings["RequestsContext"].ConnectionString;

            EnsureDatabase.For.SqlDatabase(connectionString);

            var upgrader =
                DeployChanges.To
                    .SqlDatabase(connectionString)
                    .WithScriptsEmbeddedInAssembly(typeof(Migrations.AssemblyMarker).Assembly)
                    .LogToConsole()
                    .Build();

            var result = upgrader.PerformUpgrade();

            if (!result.Successful)
            {
                throw new Exception("Could not initialize database.", result.Error);
            }

            // Web API configuration and services
            var builder = new ContainerBuilder();

            builder.RegisterType<RequestRepository>().As<IRequestRepository>();
            builder.RegisterType<RequestService>().As<IRequestService>().AsSelf();
            builder.RegisterType<XmlService>().As<IXmlService>();
            builder.RegisterType<FileService>().As<IFileService>();
            builder.RegisterType<BackgroundJobClient>().As<IBackgroundJobClient>();

            if (HttpContext.Current != null)
            { 
                builder.RegisterInstance(
                    new Paths
                    {
                        DataBaseFolderPath = HttpContext.Current.Server.MapPath("~/App_Data")
                    })
                    .AsSelf();
            } else
            {
                builder.RegisterInstance(
                    new Paths
                    {
                        DataBaseFolderPath = Paths.DefaultDataBaseFolderPath
                    })
                    .AsSelf();
            }

            builder.RegisterApiControllers(typeof(AssemblyMarker).Assembly);

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            // Hangfire
            Hangfire.GlobalConfiguration.Configuration.UseSqlServerStorage("RequestsContext");
            Hangfire.GlobalConfiguration.Configuration.UseAutofacActivator(container);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "Index",
                routeTemplate: "{id}.html",
                defaults: new { id = "index" });

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
