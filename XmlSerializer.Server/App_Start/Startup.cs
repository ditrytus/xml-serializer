﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Hangfire;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(XmlSerializer.Server.Startup))]

namespace XmlSerializer.Server
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var configuration = new HttpConfiguration();
            WebApiConfig.Register(configuration);

            app.UseWebApi(configuration);

            app
                .UseHangfireServer()
                .UseHangfireDashboard();
        }
    }
}
