﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using XmlSerializer.Common.Models;

namespace XmlSerializer.Server.Services
{
    public class XmlService : IXmlService
    {
        public XElement ToXElement(RequestModel model)
        {
            IEnumerable<XElement> GetContentChildElements()
            {
                yield return new XElement("name", model.Name);
                if (model.Visits.HasValue)
                {
                    yield return new XElement("visits", model.Visits.Value);
                }
                yield return new XElement("dateRequested", model.Date.ToString("yyyy-MM-dd"));
            };

            return new XElement("request",
                new XElement("ix", model.Index),
                new XElement("content", GetContentChildElements().Cast<object>().ToArray()));
        }

        public string ToString(XElement element)
        {
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings xws = new XmlWriterSettings()
            {
                Indent = true
            };

            using (XmlWriter xw = XmlWriter.Create(sb, xws))
            {
                element.Save(xw);
            }

            return sb.ToString();
        }
    }
}