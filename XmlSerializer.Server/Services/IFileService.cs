﻿using XmlSerializer.Common.Models;

namespace XmlSerializer.Server.Services
{
    public interface IFileService
    {
        string GetPath(RequestModel request);

        void SaveTextFile(string path, string content);
    }
}