﻿using System.Xml.Linq;
using XmlSerializer.Common.Models;

namespace XmlSerializer.Server.Services
{
    public interface IXmlService
    {
        XElement ToXElement(RequestModel request);

        string ToString(XElement element);
    }
}