﻿using System.IO;
using XmlSerializer.Common.Models;

namespace XmlSerializer.Server.Services
{
    public class FileService : IFileService
    {
        private readonly Paths paths;

        public FileService(Paths paths)
        {
            this.paths = paths;
        }

        public string GetPath(RequestModel request) =>
            $"{paths.DataBaseFolderPath}\\xml\\{request.Date:yyyy-MM-dd}\\{request.Id}.xml";

        public void SaveTextFile(string path, string content)
        {
            if (File.Exists(path))
            {
                return;
            }

            var directory = Path.GetDirectoryName(path);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            
            File.WriteAllText(path, content);
        }
    }
}