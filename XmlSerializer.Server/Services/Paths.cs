﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XmlSerializer.Server.Services
{
    public class Paths
    {
        public virtual string DataBaseFolderPath { get; set; }

        public static string DefaultDataBaseFolderPath { get; set; }
    }
}