﻿using Hangfire;
using System.Linq;
using System.Threading.Tasks;
using XmlSerializer.Common.Models;
using XmlSerializer.Server.Repositories;

namespace XmlSerializer.Server.Services
{
    public class RequestService : IRequestService
    {
        readonly IRequestRepository repository;
        readonly IBackgroundJobClient backgroundJobClient;
        readonly IXmlService xmlService;
        readonly IFileService fileService;

        const int PageSize = 30;

        public RequestService(
            IRequestRepository repository,
            IBackgroundJobClient backgroundJobClient,
            IXmlService xmlService,
            IFileService fileService)
        {
            this.repository = repository;
            this.backgroundJobClient = backgroundJobClient;
            this.xmlService = xmlService;
            this.fileService = fileService;
        }

        public async Task AddRequests(RequestModel[] requests)
        {
            await repository.InsertRequests(requests);
        }

        public void EnqueueSaveAllRequestsAsXml()
        {
            backgroundJobClient.Enqueue(() => SaveAllRequestsAsXml());
        }

        public void SaveAllRequestsAsXml()
        {
            RequestModel[] requests;
            while ((requests = repository.GetUnsavedRequests(PageSize)).Any())
            {
                foreach(var request in requests)
                {
                    backgroundJobClient.Enqueue(() => SaveRequestAsXml(request.Id));
                    request.WasSaved = true;
                }

                repository.UpdateRequests(requests);
            }
        }

        public void SaveRequestAsXml(int requestId)
        {
            var request = this.repository.GetById(requestId);
            var xelement = this.xmlService.ToXElement(request);
            var path = fileService.GetPath(request);
            fileService.SaveTextFile(path, this.xmlService.ToString(xelement));
        }
    }
}