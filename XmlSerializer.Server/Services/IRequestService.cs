﻿using System.Threading.Tasks;
using XmlSerializer.Common.Models;

namespace XmlSerializer.Server.Services
{
    public interface IRequestService
    {
        Task AddRequests(RequestModel[] request);

        void EnqueueSaveAllRequestsAsXml();
    }
}