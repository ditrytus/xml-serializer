﻿using Hangfire;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using XmlSerializer.Common.Models;

namespace XmlSerializer.Server.Data
{
    public class RequestsContext : DbContext
    {
        public RequestsContext() : this("RequestsContext") { }

        public RequestsContext(string nameOrConnectionString) : base(nameOrConnectionString) { }

        public virtual DbSet<RequestModel> Requests { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RequestModel>().ToTable("Requests");
        }
    }
}