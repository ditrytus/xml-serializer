﻿CREATE TABLE [dbo].[Requests]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Index] INT NOT NULL, 
    [Name] NVARCHAR(MAX) NOT NULL, 
    [Visits] INT NULL, 
    [Date] DATE NOT NULL
)
